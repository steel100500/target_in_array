from typing import List, Tuple
import itertools


def find_all_pairs(l, target) -> List[Tuple]:
    res = []
    for i in itertools.combinations(my_list,2):
        if target == i[0]+i[1]:
            res.append(i)
    return res


if __name__ == "__main__":
    my_list: List[int] = [9,5,1,3,8,2,12,6]
    target: int = 8
    result = find_all_pairs(my_list, target)
    if result:
        print('Found!')
        print(result)
    else:
        print("Pairs weren't found")
