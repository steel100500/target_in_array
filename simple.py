from typing import List, Tuple, Union


# only neighboring elements
def find_pair(l, target) -> Union[None, Tuple]:
    for index, value in enumerate(l):
        if index+1 < len(l):
            if target == value + l[index+1]:
                res = (value, l[index+1])
                return res
    return None


if __name__ == "__main__":
    my_list: List[int] = [9,5,3,8,2,12,6]
    target: int = 8
    result = find_pair(my_list, target)
    if result:
        print(result)
    else:
        print("A suitable pair wasn't found")