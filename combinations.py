import itertools
from typing import List, Tuple

def find_all_combinations(l, target) -> List[Tuple]:
    res = []
    for i in itertools.permutations(my_list,2):
        if target == i[0]+i[1]:
            res.append(i)
    return res


if __name__ == "__main__":
    my_list: List[int] = [9,5,1,3,8,2,12,6]
    target: int = 8
    result = find_all_combinations(my_list, target)
    if result:
        print('Found!')
        print(result)
    else:
        print("A combination wasn't found")